const Kafka = require('node-rdkafka');
const eventType = require('../producer/eventType');

const stream = Kafka.Producer.createWriteStream(
    {'metadata.broker.list': 'localhost:9092'}, 
    {}, 
    {topic: 'test-djdhar'}
);

stream.on('error', (err) => {
  console.error('Error occurred in kafka stream : ' + err);
});

function sendMessage() {
  const event = { name: "Dibyajyoti Dhar", roll_no: 67 };

  stream.write(eventType.toBuffer(event)) ?    
  console.log(`message queued (${JSON.stringify(event)})`) :
  console.log('Unable to send message..');
}

setInterval(() => {
    sendMessage();
}, 2000);