const Kafka = require('node-rdkafka');
const eventType = require('../consumer/eventType');

var consumer = new Kafka.KafkaConsumer(
    {
        'group.id': 'kafka',
        'metadata.broker.list': 'localhost:9092'
    }, 
    {}
);
  
  consumer.connect();
  
  consumer.on('ready', () => {
    console.log('Consumer is Ready..')
    consumer.subscribe(['test-djdhar']);
    consumer.consume();
  }).on('data', function(data) {
    console.log(`Received message successfully: ${eventType.fromBuffer(data.value)}`);
  });